package testeditor.gui.services.exceptions;

/**

 */
public class SaveQuestionException extends Exception {
    public SaveQuestionException(String message) {
        super(message);
    }
}